var searchData=
[
  ['neighbour_5fborder_45',['neighbour_border',['../board_8h.html#acfa4825c33f62e1a0c431c9376f3add2',1,'board.c']]],
  ['neighbour_5fcorner_46',['neighbour_corner',['../board_8h.html#a05bb3c1c8c8b6baf661ae5731571381d',1,'board.c']]],
  ['neighbour_5fintern_47',['neighbour_intern',['../board_8h.html#a4233ccabd5e4ed73dd7549e4d82b05d2',1,'board.c']]],
  ['neighbours_48',['neighbours',['../structneighbours.html',1,'']]],
  ['nzmax_49',['nzmax',['../server_8h.html#a784d8172fe2b3034810901b6ca660492',1,'server.c']]],
  ['nzmax_5fdonut_50',['nzmax_donut',['../board_8h.html#a741fe4b17d4952af5130309ed087aa95',1,'board.c']]],
  ['nzmax_5fhgraph_51',['nzmax_hgraph',['../board_8h.html#a4e0e72e444e1057506f886fa3ec80b61',1,'board.c']]],
  ['nzmax_5fsquare_52',['nzmax_square',['../board_8h.html#a0ebe3810553fd38f84b41c5c342e8886',1,'board.c']]],
  ['nzmax_5ftorus_53',['nzmax_torus',['../board_8h.html#ac9b531a73c896ba313420f62ed312840',1,'board.c']]]
];
