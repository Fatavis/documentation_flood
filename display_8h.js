var display_8h =
[
    [ "convert", "display_8h.html#abdf6d50f75aac615dc42f6171424a234", null ],
    [ "donut_case", "display_8h.html#a0f892b0badd920615e524868f762134e", null ],
    [ "float_abs", "display_8h.html#aa2dca99087e72319bad7e0b3b27f7525", null ],
    [ "graph_disp", "display_8h.html#abbdf5001aa41d45bbe4ca3b9218bd859", null ],
    [ "graph_type", "display_8h.html#a4f164715f9ba9f1c0547df1ef57b1559", null ],
    [ "hgraph_case", "display_8h.html#adf845ecb71a8759d4fe22ce03fcc7bb5", null ],
    [ "matrix_m", "display_8h.html#ab7baf9f01430e01235501edc6999bd97", null ],
    [ "rgb_color", "display_8h.html#ae22930d2e9233441cf257a5848469554", null ],
    [ "square_and_torus_case", "display_8h.html#a7af91b36fada5584b6c1eb322896fc6d", null ]
];