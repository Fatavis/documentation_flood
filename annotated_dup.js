var annotated_dup =
[
    [ "board", "structboard.html", "structboard" ],
    [ "color_set_t", "structcolor__set__t.html", "structcolor__set__t" ],
    [ "element", "structelement.html", "structelement" ],
    [ "graph_t", "structgraph__t.html", "structgraph__t" ],
    [ "move_t", "structmove__t.html", "structmove__t" ],
    [ "neighbours", "structneighbours.html", "structneighbours" ],
    [ "player", "structplayer.html", "structplayer" ],
    [ "queue", "structqueue.html", "structqueue" ]
];